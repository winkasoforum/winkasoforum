﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Util;
using Firebase.Messaging;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace Inkassoforum.Droid.Services
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseMsgService";
        public override void OnMessageReceived(RemoteMessage message)
        {
            Log.Debug(TAG, "From: " + message.From);
            if (message.GetNotification() != null)
            {
                //These is how most messages will be received
                Log.Debug(TAG, "Notification Message Body: " + message.GetNotification().Body);
                SendNotification(message.GetNotification().Body, "Concent InkasoForum");
            }
            else
            {
                //Only used for debugging payloads sent from the Azure portal
                SendNotification(message.Data["message"], message.Data["title"]);
            }
        }

        void SendNotification(string messageBody, string title)
        {
            using (var notificationManager = NotificationManager.FromContext(BaseContext))
            {
                Notification notification;
                var myUrgentChannel = PackageName;
                const string channelName = "SushiHangover Urgent";

                NotificationChannel channel;
                channel = notificationManager.GetNotificationChannel(myUrgentChannel);
                if (channel == null)
                {
                    channel = new NotificationChannel(myUrgentChannel, channelName, NotificationImportance.High);
                    channel.EnableVibration(true);
                    channel.EnableLights(true);
                    channel.LockscreenVisibility = NotificationVisibility.Public;
                    notificationManager.CreateNotificationChannel(channel);
                }
                channel?.Dispose();


                notification = new Notification.Builder(BaseContext)
                    .SetChannelId(myUrgentChannel)
                    .SetContentTitle(title)
                    .SetContentText(messageBody)
                    .SetAutoCancel(true)
                    .SetSmallIcon(Resource.Drawable.concenticon)
                    .Build();

                notificationManager.Notify(1331, notification);
                notification.Dispose();
            }
        }
        
    }
}