﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inkassoforum.Models
{
    public class ScheduleItem
    {
        public string Id { get; set; }
        public string EventTtile { get; set; }
        public DateTime EventStartTime { get; set; }
        public string EventDescription { get; set; }
        public DateTime EventEndTime { get; set; }
        public string EventPlace { get; set; }
        public string EventPresenter { get; set; }
        public string EventTags { get; set; }
        public string EventStartTimeString
        {
            get
            {
                return EventStartTime.ToString("HH:mm") ;
            }
        }
        public string EventEndTimeString
        {
            get
            {
                return "- " + EventEndTime.ToString("HH:mm");
            }
        }
    }
}
