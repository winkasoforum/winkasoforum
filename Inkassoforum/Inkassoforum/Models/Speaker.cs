﻿namespace Inkassoforum.Models
{
    public class Speaker
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PhotoFileName { get; set; }
        public string Text => $"{Name}\n\n{Description}";
    }
}