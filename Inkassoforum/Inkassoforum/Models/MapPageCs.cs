﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Inkassoforum.Models
{
    public class MapPageCS : ContentPage
    {
        public MapPageCS()
        {
            var customMap = new CustomMap
            {
                MapType = MapType.Satellite,
                WidthRequest = App.ScreenWidth,
                HeightRequest = App.ScreenHeight
            };

            var pin = new CustomPin
            {
                Type = PinType.Place,
                Position = new Position(59.176439, 10.208144),
                Label = "Inkassoforum 2018",
                Address = "Fokserødveien 12, 3241 Sandefjord, Norway",
                Id = "Concent",
                Url = "https://concent.no/"
            };

            var pin2 = new CustomPin
            {
                Type = PinType.Place,
                Position = new Position(59.177319, 10.202608),
                Label = "After Party!",
                Address = "Somewhere near...",
                Id = "Party",
                Url = "https://concent.no/"
            };

            customMap.CustomPins = new List<CustomPin> { pin, pin2 };
            customMap.Pins.Add(pin);
            customMap.Pins.Add(pin2);
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(59.177670, 10.208134), Distance.FromKilometers(2.0)));

            Content = customMap;
        }
    }
}
