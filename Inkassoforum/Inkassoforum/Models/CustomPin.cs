﻿using Xamarin.Forms.Maps;

namespace Inkassoforum.Models
{
    public class CustomPin : Pin
    {
        public string Id { get; set; }

        public string Url { get; set; }
    }
}
