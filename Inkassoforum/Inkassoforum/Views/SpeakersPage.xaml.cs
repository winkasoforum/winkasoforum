﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Inkassoforum.Models;
using Inkassoforum.Views;
using Inkassoforum.ViewModels;

namespace Inkassoforum.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpeakersPage : ContentPage
    {
        SpeakersViewModel viewModel;        

        public SpeakersPage()
        {
            InitializeComponent();            

            BindingContext = viewModel = new SpeakersViewModel();
        }

        //async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        //{
        //    var item = args.SelectedItem as Speaker;
        //    if (item == null)
        //        return;

        //    await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));

        //    // Manually deselect item.
        //    ItemsListView.SelectedItem = null;
        //}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Speakers.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

        protected override bool OnBackButtonPressed()
        {
            Application.Current.MainPage = new NavigationPage(new FirstPage());
            return true;
        }

    }
}