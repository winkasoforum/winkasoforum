﻿using Inkassoforum.Models;
using Inkassoforum.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inkassoforum.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScheduleItemPage : ContentPage
	{
        ScheduleViewModel scheduleViewModel;
        public ScheduleItemPage()
        {
            InitializeComponent();
            BindingContext = scheduleViewModel = new ScheduleViewModel();

        }
        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as ScheduleItem;
            if (item == null)
                return;

            await Navigation.PushAsync(new EventDetailsPage3(new ScheduleDetailsViewModel(item)));

            // Manually deselect item.
            ScheduleListView.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (scheduleViewModel.ScheduleItems.Count == 0)
                scheduleViewModel.LoadItemsCommand.Execute(null);
        }
    }
}