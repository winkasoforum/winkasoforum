﻿using Inkassoforum.Models;
using Inkassoforum.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inkassoforum.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SchedulePage : TabbedPage
    {
        protected override bool OnBackButtonPressed()
        {
            Application.Current.MainPage = new NavigationPage(new FirstPage());
            return true;
        }

        ScheduleViewModel scheduleViewModel;
        public SchedulePage ()
		{
			InitializeComponent();

        }
    }
}