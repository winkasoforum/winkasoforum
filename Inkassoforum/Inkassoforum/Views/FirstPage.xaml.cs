﻿using Inkassoforum.Models;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inkassoforum.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FirstPage : ContentPage
    {
        public FirstPage()
        {
            InitializeComponent();
        }

        void OnAgendaTapped(object sender, EventArgs args)
        {
            Application.Current.MainPage = new NavigationPage(new SchedulePage());
        }

        void OnSpeakersTapped(object sender, EventArgs args)
        {
            Application.Current.MainPage = new NavigationPage(new SpeakersPage());
        }

        void OnMapTapped(object sender, EventArgs args)
        {
            Application.Current.MainPage = new NavigationPage(new MapPageCS());
        }

    }
}