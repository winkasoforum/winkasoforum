﻿using Inkassoforum.Models;
using Inkassoforum.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inkassoforum.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EventDetailPage : ContentPage
	{
        ScheduleDetailsViewModel viewModel;

        public EventDetailPage(ScheduleDetailsViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        public EventDetailPage()
        {
            InitializeComponent();

            var item = new ScheduleItem
            {
                EventTtile = viewModel.scheduleItem.EventTtile,
                EventDescription = viewModel.scheduleItem.EventDescription,
                EventPresenter = viewModel.scheduleItem.EventPresenter,
            };

            viewModel = new ScheduleDetailsViewModel(item);
            BindingContext = viewModel;
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NavigationPage(new QAPage(viewModel)));
        }
    }
}