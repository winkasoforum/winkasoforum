﻿using Inkassoforum.Models;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Inkassoforum.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
		public MapPage ()
		{
			InitializeComponent ();

            var pin = new Models.CustomPin
            {
                Type = PinType.Place,
                Position = new Position(59.176439, 10.208144),
                Label = "Inkassoforum 2018",
                Address = "Fokserødveien 12, 3241 Sandefjord, Norway",
                Id = "Concent",
                Url = "https://concent.no/"
            };

            var pin2 = new CustomPin
            {
                Type = PinType.Place,
                Position = new Position(59.177319, 10.202608),
                Label = "After Party!",
                Address = "Somewhere near...",
                Id = "Party",
                Url = "https://concent.no/"
            };

            customMap.CustomPins = new List<CustomPin> { pin, pin2 };
            customMap.Pins.Add(pin);
            customMap.Pins.Add(pin2);
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(59.176439, 10.208144), Distance.FromKilometers(2.0)));
        }
	}
}