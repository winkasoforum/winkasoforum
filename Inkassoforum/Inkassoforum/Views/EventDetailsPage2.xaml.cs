﻿using Inkassoforum.Models;
using Inkassoforum.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inkassoforum.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EventDetailsPage2 : ContentPage
    {
        ScheduleDetailsViewModel viewModel;
        public EventDetailsPage2()
        {
            InitializeComponent();
        }

        public EventDetailsPage2(ScheduleDetailsViewModel viewModel)
        {
            try
            {
                InitializeComponent();

                BindingContext = this.viewModel = viewModel;
            }
            catch(Exception ex)
            {
                string ed = ex.Message;
            }
           
        }
        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NavigationPage(new QAPage(viewModel)));
        }
    }
}