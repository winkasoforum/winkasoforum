﻿using Inkassoforum.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Inkassoforum.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventDetailsPage3 : ContentPage
    {
        ScheduleDetailsViewModel scheduleDetailsViewModel;

        public EventDetailsPage3()
        {
            InitializeComponent();
        }
        public EventDetailsPage3(ScheduleDetailsViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = scheduleDetailsViewModel = viewModel;
        }
        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NavigationPage(new QAPage(scheduleDetailsViewModel)));
        }
    }
}