﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Inkassoforum.ViewModels;
using Inkassoforum.Models;
using Inkassoforum.Services;

namespace Inkassoforum.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QAPage : ContentPage
	{
        ScheduleDetailsViewModel scheduleDetails;
        public QAPage(ScheduleDetailsViewModel scheduleDetails)
		{
			InitializeComponent();
            BindingContext = this.scheduleDetails = scheduleDetails;            
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            //MessagingCenter.Send(this, "AddItem", Item);; 
            //await Navigation.PopModalAsync();
        }

        private void Send_Clicked(object sender, EventArgs e)
        {            
            QuestionSenderController controller = new QuestionSenderController();
            try
            {
                controller.SendQuestion(new QAQuestion { presentationTitle = scheduleDetails.scheduleItem.EventTtile, qustion = this.questionField.Text });
                DisplayAlert("Question sent!", "Thank you, your question have been sent to our speaker.", "OK");
            }
            catch
            {
                DisplayAlert("Question was not sent!", "Something went wrong, please try again.", "OK");
            }           
         }
    }
}