﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Inkassoforum.Models;

namespace Inkassoforum.Services
{
    class QuestionSenderController
    {
        private SqlConnection _connection;
        public QuestionSenderController()
        {
            _connection = new SqlConnection("Server=concenthackatondev.database.windows.net ;Database=ConcentIncasoforumDEV;User Id=concentdev;Password = Test123!;");
        }

        public void SendQuestion(QAQuestion question)
        {
            SqlCommand command = new SqlCommand();
            command.CommandText = "INSERT INTO QAquestions VALUES (@presentation, @question)";
            command.Connection = _connection;
            command.Parameters.AddWithValue("@presentation", question.presentationTitle);
            command.Parameters.AddWithValue("@question", question.qustion);
            _connection.Open();
            command.ExecuteNonQuery();
            _connection.Close();
        }
    }
}
