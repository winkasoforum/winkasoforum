﻿using Inkassoforum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inkassoforum.Services
{
    public class AgendaStore:IAgendaStore<ScheduleItem>
    {
        List<ScheduleItem> items;
        public AgendaStore()
        {
            items = new List<ScheduleItem>();
            var mockItems = new List<ScheduleItem>
            {
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "First event", EventDescription="This is an item description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Second event", EventDescription="This is an event description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Third event", EventDescription="This is an event description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Fourth event", EventDescription="This is an event description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Fifth event", EventDescription="This is an event description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Sixth event", EventDescription="This is an event description." },
            };

            foreach (var item in mockItems)
            {
                items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(ScheduleItem item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(ScheduleItem item)
        {
            var oldItem = items.Where((ScheduleItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((ScheduleItem arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<ScheduleItem> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<ScheduleItem>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}
