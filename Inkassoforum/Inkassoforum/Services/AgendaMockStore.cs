﻿using Inkassoforum.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inkassoforum.Services
{
    public class AgendaMockStore : IAgendaStore<ScheduleItem>
    {
        List<ScheduleItem> items;

        public AgendaMockStore()
        {
            items = new List<ScheduleItem>();
            var mockItems = new List<ScheduleItem>
            {
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "News in Predator", EventDescription="Review of new features in Predator.", EventPresenter="Knut Skofteland", EventPlace="Osebergu HALL", EventTags="Predator", EventStartTime = DateTime.ParseExact("110001012018", "HHmmddMMyyyy", CultureInfo.InvariantCulture),EventEndTime = DateTime.ParseExact("114501012018", "HHmmddMMyyyy", CultureInfo.InvariantCulture) },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Coffee break with exhibit sweep", EventDescription="Coffee break with exhibit sweep", EventPresenter="", EventPlace="", EventTags="Coffe", EventStartTime = DateTime.ParseExact("114501012018", "HHmmddMMyyyy", CultureInfo.InvariantCulture),EventEndTime = DateTime.ParseExact("120001012018", "HHmmddMMyyyy", CultureInfo.InvariantCulture) },

            };

            foreach (var item in mockItems)
            {
                items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(ScheduleItem item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(ScheduleItem item)
        {
            var oldItem = items.Where((ScheduleItem arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((ScheduleItem arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<ScheduleItem> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<ScheduleItem>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}
