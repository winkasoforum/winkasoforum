﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inkassoforum.Models;

namespace Inkassoforum.Services
{
    public class SpeakersDataStore : IDataStore<Speaker>
    {
        readonly List<Speaker> _items;

        public SpeakersDataStore()
        {
            _items = new List<Speaker>();
            var mockItems = new List<Speaker>
            {
                new Speaker { Id = Guid.NewGuid().ToString(), Name = "Knut Skofteland", PhotoFileName = "Knut.jpg", Description = "Product Manager at CONCENT"},
                new Speaker { Id = Guid.NewGuid().ToString(), Name = "Hans Christian Solhaug", PhotoFileName = "HCS.jpg", Description = "Sales Consultant at CONCENT"},
                new Speaker { Id = Guid.NewGuid().ToString(), Name = "Marit Næss", PhotoFileName = "MN.jpg", Description = "Business Analyst at CONCENT"}
            };

            foreach (var item in mockItems)
            {
                _items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(Speaker item)
        {
            _items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Speaker item)
        {
            var oldItem = _items.Where((Speaker arg) => arg.Id == item.Id).FirstOrDefault();
            _items.Remove(oldItem);
            _items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = _items.Where((Speaker arg) => arg.Id == id).FirstOrDefault();
            _items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Speaker> GetItemAsync(string id)
        {
            return await Task.FromResult(_items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Speaker>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(_items);
        }
    }
}