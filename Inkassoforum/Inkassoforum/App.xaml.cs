﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Inkassoforum.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Inkassoforum
{
    public partial class App : Application
    {

        public static double ScreenHeight;
        public static double ScreenWidth;

        public App()
        {
            InitializeComponent();

            MainPage = new FirstPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
