﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using Inkassoforum.Models;
using Inkassoforum.Views;

namespace Inkassoforum.ViewModels
{
    public class SpeakersViewModel : BaseViewModel
    {
        public ObservableCollection<Speaker> Speakers { get; set; }
        public Command LoadItemsCommand { get; set; }

        public SpeakersViewModel()
        {
            Title = "Speakers";
            Speakers = new ObservableCollection<Speaker>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Speakers.Clear();
                var items = await SpeakersStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Speakers.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}