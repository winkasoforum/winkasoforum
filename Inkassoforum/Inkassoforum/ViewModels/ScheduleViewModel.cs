﻿using Inkassoforum.Models;
using Inkassoforum.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Inkassoforum.ViewModels
{
    public class ScheduleViewModel : BaseViewModel
    {
        public ObservableCollection<ScheduleItem> ScheduleItems { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ScheduleViewModel()
        {
            Title = "Agenda";
            ScheduleItems = new ObservableCollection<ScheduleItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                ScheduleItems.Clear();
                var items = await AgendaStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    ScheduleItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
