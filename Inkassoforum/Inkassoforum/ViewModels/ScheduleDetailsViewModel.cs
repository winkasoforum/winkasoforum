﻿using Inkassoforum.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inkassoforum.ViewModels
{
    public class ScheduleDetailsViewModel : BaseViewModel
    {
        public ScheduleItem scheduleItem { get; set; }
        public ScheduleDetailsViewModel(ScheduleItem item)
        {
            scheduleItem = item;
        }
    }
}
