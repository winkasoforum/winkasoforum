﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inkasoforum.API.Models
{
    public class QAquestionDTO
    {
        public string Presentation { get; set; }
        public string Question { get; set; }
        public Int64 Id { get; set; }
    }
}