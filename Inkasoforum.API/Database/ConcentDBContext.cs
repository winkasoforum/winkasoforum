﻿using Inkasoforum.API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Inkasoforum.API.Database
{
    public class ConcentDBContext : DbContext
    {
        public ConcentDBContext() : base("ConcentContext")
        {
        }

        public DbSet<QAquestionDTO> Questions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<QAquestionDTO>().ToTable("QAquestions");
        }
    }
}