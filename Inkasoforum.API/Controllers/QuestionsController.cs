﻿using Inkasoforum.API.Database;
using Inkasoforum.API.Models;
using Inkassoforum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inkasoforum.API.Controllers
{
    public class QuestionsController : ApiController
    {
        public IEnumerable<QAquestionDTO> GetQuestions()
        {
            List<QAquestionDTO> questions = new List<QAquestionDTO>();
            using (ConcentDBContext db = new ConcentDBContext())
            {
                return db.Questions.ToList();
            }
        }

    }
}
