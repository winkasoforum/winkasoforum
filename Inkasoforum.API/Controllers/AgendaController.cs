﻿using Inkassoforum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inkasoforum.API.Controllers
{
    public class AgendaController : ApiController
    {
        public IEnumerable<ScheduleItem> GetEvents()
        {
            var mockItems = new List<ScheduleItem>
            {
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "First event", EventDescription="This is an item description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Second event", EventDescription="This is an event description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Third event", EventDescription="This is an event description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Fourth event", EventDescription="This is an event description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Fifth event", EventDescription="This is an event description." },
                new ScheduleItem { Id = Guid.NewGuid().ToString(), EventTtile = "Sixth event", EventDescription="This is an event description." },
            };
            return mockItems;
        }

    }
}
